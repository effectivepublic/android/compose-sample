package band.effective.headlines.compose.network.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class NewsPageResultDto(
    @Json(name = "articles")
    val articles: List<ArticleDto>
)

@JsonClass(generateAdapter = true)
class ArticleDto(
    @Json(name = "title")
    val title: String,
    @Json(name = "description")
    val description: String?,
    @Json(name = "source")
    val source: ArticleSourceDto,
    @Json(name = "url")
    val url: String,
    @Json(name = "urlToImage")
    val imageUrl: String?,
    @Json(name = "content")
    val content: String?,
    @Json(name = "publishedAt")
    val date: Date
)

@JsonClass(generateAdapter = true)
class ArticleSourceDto(
    @Json(name = "name")
    val name: String
)
