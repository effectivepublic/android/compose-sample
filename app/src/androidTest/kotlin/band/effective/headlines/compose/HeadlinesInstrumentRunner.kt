package band.effective.headlines.compose

import android.app.Application
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.test.runner.AndroidJUnitRunner

@Suppress("unused")
class HeadlinesInstrumentRunner : AndroidJUnitRunner() {


    override fun onCreate(arguments: Bundle) {
        super.onCreate(arguments)
    }

    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, InstrumentApplication::class.java.name, context)
    }
}