package band.effective.headlines.compose.search

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import band.effective.headlines.compose.NavigationBarItem
import band.effective.headlines.compose.core.utils.ruleChain
import band.effective.headlines.compose.presentation.AppHost
import band.effective.headlines.compose.search.presentation.SearchScreen
import band.effective.headlines.compose.search.presentation.SearchScreenNavigation
import band.effective.headlines.compose.search.presentation.models.SearchItemNavArg
import band.effective.headlines.compose.ui.theme.HeadlinesComposeTheme
import band.effective.headlines.doRetryable
import band.effective.headlines.compose.network.HeadlinesMockBackendRule
import band.effective.headlines.compose.network.SearchBackend
import band.effective.headlines.compose.network.dto.ArticleDto
import band.effective.headlines.compose.network.dto.ArticleSourceDto
import band.effective.headlines.compose.network.dto.NewsPageResultDto
import com.google.accompanist.insets.ProvideWindowInsets
import org.junit.Rule
import org.junit.Test
import java.util.*

class SearchScreenTest {

    private val mockBackendRule = HeadlinesMockBackendRule()
    private val composeRule = createComposeRule()

    @get:Rule
    val rule = ruleChain {
        +mockBackendRule
        +composeRule
    }

    /**
     * Поиск новостей
     */
    @Test
    fun newsSearch() {
        val searchBackend = SearchBackend() //mockBackendRule[SearchBackend::class]
        searchBackend.putSearchResponse(
            "omsk",
            NewsPageResultDto(
                articles = listOf(
                    generateArticleDto("Omsk Summit"),
                    generateArticleDto(
                        "DevFest Omsk",
                        "DevFest is a global, decentralized tech conference hosted by the Google Developer Groups (GDG) community"
                    ),
                )
            ),
        )

        // Step 1. Перейти на экран поиска
        openApp()
        NavigationBarItem(composeRule).search.performClick()
        val searchScreen = SearchScreen(composeRule)

        // Step 2. Ввести поисковой запрос "omsk"
        searchScreen.searchField.performTextInput("omsk")

        // Expected. Дернулась ручка /everything/q=omsk&page=0&pageSize=20&sortBy=popularity
        searchBackend.assertSearch(page = "0", text = "omsk")

        // Expected. Отобразились сущности из ответа ручки
        doRetryable {
            composeRule.onNodeWithText("Omsk Summit").assertExists()
            composeRule.onNodeWithText("DevFest Omsk").assertExists()
        }
    }

    private fun generateArticleDto(
        title: String,
        description: String? = null
    ): ArticleDto {
        return ArticleDto(
            title = title,
            description = description,
            source = ArticleSourceDto("effective news"),
            url = "effective.band",
            imageUrl = null,
            content = null,
            date = Date(),
        )
    }

    private fun openApp() {
        composeRule.setContent {
            HeadlinesComposeTheme() {
                ProvideWindowInsets {
                    AppHost()
                }
            }
        }
    }

    private fun openSearchScreen() {
        composeRule.setContent {
            HeadlinesComposeTheme() {
                SearchScreen(object : SearchScreenNavigation {
                    override fun openArticleDetails(searchItemNavArg: SearchItemNavArg) {}
                })
            }
        }
    }
}
