package band.effective.headlines.compose.search

import androidx.compose.ui.test.junit4.ComposeContentTestRule
import androidx.compose.ui.test.onNodeWithTag


class SearchScreen(private val composeRule: ComposeContentTestRule) {

    val searchField get() = composeRule.onNodeWithTag("search_field")
}
