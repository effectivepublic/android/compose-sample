package band.effective.headlines

import android.os.SystemClock

const val DEFAULT_TIMEOUT = 1000L * 10
const val DEFAULT_INTERVAL = 100L

/**
 * Prevents retry for inner [doRetryable] calls
 */
private val outerCall: ThreadLocal<Boolean> = ThreadLocal()

/**
 * Performs [action] every [interval] until one of the following will become true:
 * - action will complete successfully without any Exception;
 * - timeout will happen after [timeout].
 *
 * Typically should be used to provide auto retryable behaviour for any action
 *
 * Example:
 * ```
 * doRetryable {
 *      onView { withId(R.id.view_id) }.assert{ isDisplayed() }
 *      onView { withId(R.id.another_view_id) }.assert{ isClickable() }
 * }
 * ```
 *
 * Nested [doRetryable] are not retrying:
 * ```
 * doRetryable(100, 10) {
 *     doRetryable(100, 5) { assertTrue(false) }
 * }
 * ```
 * will be execute 10 times (based on outer parameters of [doRetryable])
 */
fun <T> doRetryable(
    timeout: Long = DEFAULT_TIMEOUT,
    interval: Long = DEFAULT_INTERVAL,
    action: () -> T
): T {
    if (outerCall.get() == true) {
        return action()
    }
    outerCall.set(true)

    var elapsedTime = 0L
    var startActionTime = 0L

    while (true) {
        try {
            startActionTime = SystemClock.elapsedRealtime()
            val result = action()
            outerCall.set(false)
            return result
        } catch (e: Throwable) {
            elapsedTime += SystemClock.elapsedRealtime() - startActionTime
            elapsedTime += interval
            if (elapsedTime >= timeout) {
                outerCall.set(false)
                throw e
            }
            Thread.sleep(interval)
        }
    }
}
