package band.effective.headlines.compose.network

import com.squareup.moshi.Moshi
import okhttp3.mockwebserver.MockResponse

inline fun <reified T : Any> T.respondMoshi200(moshi: Moshi): MockResponse {
    return MockResponse().setResponseCode(200).setBody(moshi.adapter(T::class.java).toJson(this))
}


fun respond500(): MockResponse {
    return MockResponse().setResponseCode(500)
}
