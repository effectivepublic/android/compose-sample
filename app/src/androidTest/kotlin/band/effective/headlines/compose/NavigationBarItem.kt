package band.effective.headlines.compose

import androidx.compose.ui.test.junit4.ComposeContentTestRule
import androidx.compose.ui.test.onNodeWithTag

class NavigationBarItem(private val composeRule: ComposeContentTestRule) {

    val feed get() = composeRule.onNodeWithTag("feed")
    val search get() = composeRule.onNodeWithTag("search")
    val about get() = composeRule.onNodeWithTag("about")
    val root get() = composeRule.onNodeWithTag("root")
}
