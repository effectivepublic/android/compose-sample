package band.effective.headlines.compose.network

import band.effective.headlines.compose.network.verifier.test
import band.effective.headlines.compose.network.verifier.url
import band.effective.headlines.compose.network.verifier.verifyBackend
import band.effective.headlines.compose.network.dto.NewsPageResultDto
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.addAdapter
import okhttp3.mockwebserver.MockResponse
import org.junit.Assert

open class SearchBackend : HeadlineMusicBackend() {

    companion object {

        private val SEARCH_REGEX = Regex("everything.*")
    }

    private var newsSearch = mutableMapOf<String, MutableMap<Int, NewsPageResultDto>>()

    @OptIn(ExperimentalStdlibApi::class)
    private val moshi = Moshi.Builder()
        .addAdapter(Rfc3339DateJsonAdapter().nullSafe())
        .build()

    override fun setup(builder: BackendMockBuilder) {

        /**
         * Mock for [band.effective.headlines.compose.news_api.data.headlines.remote.NewsApiDataSource]
         */
        builder.bindRequest(SEARCH_REGEX, Responder.RequestMethod.GET, this::search)
    }

    fun assertSearch(
        page: String,
        text: String,
    ) {
        verifyBackend {
            endpoint(SEARCH_REGEX, Responder.RequestMethod.GET) with {
                url {
                    queryParams["text"] == text
                }
            } test {
                url {
                    Assert.assertEquals(page, queryParams["page"])
                }
            }
        }
    }

    fun putSearchResponse(query: String, response: NewsPageResultDto, page: Int = 0) {
        val result = newsSearch.getOrPut(query) { mutableMapOf() }
        result[page] = response
    }

    private fun search(mockContext: MockContext): MockResponse {
        val query = mockContext.originalRequest.url.queryParameter("q")!!
        val page = mockContext.originalRequest.url.queryParameter("page")!!.toInt()

        return newsSearch[query]?.get(page)
            ?.respondMoshi200(moshi)
            ?: respond500()
    }
}
