package band.effective.headlines.compose

import band.effective.headlines.compose.network.MockBackendRule
import band.effective.headlines.compose.network.SearchBackend

class InstrumentApplication : HeadlinesComposeApp() {

    override fun onCreate() {
        super.onCreate()

        initStaticBackends()
    }

    private fun initStaticBackends() {
        val backends = listOf(
            SearchBackend::class
        )
        //MockBackendRule.initStaticBackends(backends)
    }
}
