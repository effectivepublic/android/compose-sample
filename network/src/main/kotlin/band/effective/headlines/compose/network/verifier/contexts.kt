package band.effective.headlines.compose.network.verifier

import band.effective.headlines.compose.core.utils.filterNotNullValues
import okhttp3.HttpUrl
import okhttp3.mockwebserver.RecordedRequest


class UrlContext(
    private val request: RecordedRequest
) {

    val url: HttpUrl by lazy { request.requestUrl!! }
    val queryParams: Map<String, String> by lazy { url.queryMap() }
}

private fun HttpUrl.queryMap(): Map<String, String> {
    return queryParameterNames.associateWith { queryParameter(it) }.filterNotNullValues()
}
