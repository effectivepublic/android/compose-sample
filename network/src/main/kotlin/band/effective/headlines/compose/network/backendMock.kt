package band.effective.headlines.compose.network

import androidx.core.content.ContextCompat
import band.effective.headlines.compose.core.utils.targetContext
import net.bytebuddy.ByteBuddy
import net.bytebuddy.android.AndroidClassLoadingStrategy
import net.bytebuddy.implementation.MethodDelegation.to
import net.bytebuddy.implementation.bind.annotation.RuntimeType
import net.bytebuddy.implementation.bind.annotation.SuperCall
import net.bytebuddy.matcher.ElementMatchers.isPublic
import okhttp3.mockwebserver.MockResponse
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import java.util.concurrent.Callable
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

/**
 * Mock backend is an interface for instances of helper classes that simulates backend.
 * Used to facilitate complex behavior of mocks that is impossible to do with plain static ones
 */
interface BackendMock {

    fun setup(builder: BackendMockBuilder)
}

/**
 * Helper to sync backends
 * To make it work backend should be open class in kotlin or NOT final in Java.
 * All magic happens with reflection and interception public methods by CommonLockInterceptor.
 *
 * @param B the backend to be synced
 * @param clazz java Class of this backend
 * @return synced B class implementation
 */
fun <B> syncBackend(clazz: Class<B>): B {
    return ByteBuddy()
        .subclass(clazz)
        .method(isPublic())
        .intercept(to(CommonLockInterceptor::class.java))
        .make()
        .load(
            clazz.classLoader,
            AndroidClassLoadingStrategy.Wrapping(ContextCompat.getCodeCacheDir(targetContext))
        )
        .loaded
        .newInstance()
}

abstract class MockBackendRule : TestRule {

    private class BackendMockData(val mock: BackendMock, val tokens: List<String>)

    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {
            override fun evaluate() {
                try {
                    base.evaluate()
                    Responder.checkCollisions()?.let { throw it }
                } catch (e: Throwable) {
                    Responder.checkCollisions()?.let {
                        it.addSuppressed(e)
                        throw it
                    } ?: throw e
                } finally {
                    afterEvaluate()
                }
            }
        }
    }

    operator fun <B : BackendMock> get(klass: KClass<B>): B {
        @Suppress("UNCHECKED_CAST")
        return backends[klass]!!.mock as B
    }

    protected abstract fun afterEvaluate()

    companion object {

        private val backends = ConcurrentHashMap<KClass<out BackendMock>, BackendMockData>()

        fun initStaticBackends(staticBackends: List<KClass<out BackendMock>>) {
            staticBackends.forEach { klass ->
                val backendMock = syncBackend(klass.java)
                mockBackend(klass, backendMock)
            }
        }

        fun resetStaticBackend(klass: KClass<out BackendMock>) {
            unmockBackend(klass)
            mockBackend(klass, syncBackend(klass.java))
        }

        private fun <B : BackendMock> mockBackend(klass: KClass<out BackendMock>, backend: B) {
            val tokens = mutableListOf<String>()
            backend.setup(BackendMockBuilder(tokens))
            backends[klass] = BackendMockData(backend, tokens)
        }

        private fun unmockBackend(klass: KClass<out BackendMock>) {
            backends.remove(klass)?.let { data ->
                data.tokens.forEach { token ->
                    Responder.unmockResponse(token)
                }
            }
        }
    }
}

class BackendMockBuilder(private val tokens: MutableList<String>) {

    fun bindRequest(
        url: String,
        method: Responder.RequestMethod = Responder.RequestMethod.ANY,
        binder: (MockContext) -> MockResponse
    ) {
        tokens += Responder.mockResponse(url, method, binder)
    }

    fun bindRequest(
        url: Regex,
        method: Responder.RequestMethod = Responder.RequestMethod.ANY,
        binder: (MockContext) -> MockResponse
    ) {
        tokens += Responder.mockResponse(url, method, binder)
    }
}

object CommonLockInterceptor {
    @RuntimeType
    @Throws(Exception::class)
    open fun intercept(@SuperCall zuper: Callable<*>): Any {
        val lock = backendSharedLock
        lock.lock()
        return try {
            zuper.call()
        } finally {
            lock.unlock()
        }
    }
}
