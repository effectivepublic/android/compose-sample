package band.effective.headlines.compose.network

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import okhttp3.HttpUrl
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

typealias ResponseCallback = (MockContext) -> MockResponse
typealias ExternalRouteResponseCallback = (RecordedRequest) -> MockResponse

//Should be used to lock all backends around it
val backendSharedLock = ReentrantLock()

object Responder : MockResponder, ExternalRouteResponder, RequestRecorder {

    enum class RequestMethod {
        ANY {

            override fun matches(method: String): Boolean = true
        },
        GET {

            override fun matches(method: String): Boolean = method == "GET"
        },
        POST {

            override fun matches(method: String): Boolean = method == "POST"
        };

        abstract fun matches(method: String): Boolean
    }

    private class InterceptConditions(val requestMethod: RequestMethod, val url: Regex) {

        fun matches(method: String, tail: String): Boolean {
            return requestMethod.matches(method) && url.matches(tail)
        }

        override fun toString(): String {
            return "mock { method=$requestMethod, url=${url.pattern} }"
        }
    }

    private class InterceptData(
        val conditions: InterceptConditions,
        val callback: ResponseCallback?,
        val externalRouteCallback: ExternalRouteResponseCallback?
    )

    private val tokenNumber = AtomicLong()
    private val responses: MutableMap<String, InterceptData> = ConcurrentHashMap()

    private val collisions = ConcurrentHashMap<String, List<InterceptConditions>>()

    private val recordedRequests: MutableMap<String, List<TimedRecordedRequest>> = ConcurrentHashMap()
    private val recordedRequestFlow: MutableSharedFlow<TimedRecordedRequest> = MutableSharedFlow(extraBufferCapacity = 5)

    private fun matchingMock(request: Request): InterceptData? {
        return matchingMockWithToken(request)?.second
    }

    private fun matchingMockWithToken(request: Request): Pair<String, InterceptData>? {
        return matchingMockWithTokenInner(request.method, request.url.tail)
    }

    private fun matchingMock(request: RecordedRequest): InterceptData? {
        return matchingMockInner(request.method!!, request.requestUrl!!.tail)
    }

    private fun matchingMockInner(method: String, tail: String): InterceptData? {
        return responses.values.filter { it.conditions.matches(method, tail) }.let { mocks ->
            if (mocks.size > 1) {
                val key = "$method $tail"
                val value = mocks.map { it.conditions }

                collisions[key] = value

                error("Colliding mocks: $key: $value")
            }

            mocks.firstOrNull()
        }
    }

    private fun matchingMockWithTokenInner(method: String, tail: String): Pair<String, InterceptData>? {
        return responses
            .filterValues { it.conditions.matches(method, tail) }
            .toList()
            .let { mocks ->
                if (mocks.size > 1) {
                    val key = "$method $tail"
                    val value = mocks.map { it.second.conditions }

                    collisions[key] = value

                    error("Colliding mocks: $key: $value")
                }
                mocks.firstOrNull()
            }
    }

    override fun willRespond(request: Request): Boolean {
        return matchingMock(request) != null
    }

    override fun respond(mockContext: MockContext): MockResponse {
        return backendSharedLock.withLock {
            val (token, mock) = matchingMockWithToken(mockContext.originalRequest)!!
            recordRequest(token, mockContext.recordedRequest)
            mock.callback!!(mockContext)
        }
    }

    override fun willRespond(request: RecordedRequest): Boolean {
        return matchingMock(request) != null
    }

    override fun respond(request: RecordedRequest): MockResponse {
        return backendSharedLock.withLock {
            matchingMock(request)!!.externalRouteCallback!!(request)
        }
    }

    override fun getTimedRecordedRequests(): List<TimedRecordedRequest> {
        return recordedRequests.flatMap { (_, requests) -> requests }
    }

    override fun getTimedRecordedRequestFlow(): Flow<TimedRecordedRequest> {
        return recordedRequestFlow
    }

    fun checkCollisions(): RuntimeException? {
        if (collisions.isNotEmpty()) {
            ArrayList(collisions.entries).map {
                "${it.key}: ${it.value}"
            }.also {
                collisions.clear()
                return IllegalStateException("Colliding mocks: $it")
            }
        }

        return null
    }

    fun mockResponse(url: String, method: RequestMethod, callback: ResponseCallback): String {
        return mockResponse(Regex(Regex.escape(url)), method, callback, null)
    }

    fun mockExternalRouteResponse(
        url: String,
        method: RequestMethod,
        externalRouteCallback: ExternalRouteResponseCallback
    ): String {
        return mockResponse(Regex(Regex.escape(url)), method, null, externalRouteCallback)
    }

    fun mockResponse(url: Regex, method: RequestMethod, callback: ResponseCallback): String {
        return mockResponse(url, method, callback, null)
    }

    fun mockExternalRouteResponse(
        url: Regex,
        method: RequestMethod,
        externalRouteCallback: ExternalRouteResponseCallback
    ): String {
        return mockResponse(url, method, null, externalRouteCallback)
    }

    private fun mockResponse(
        url: Regex,
        method: RequestMethod,
        callback: ResponseCallback?,
        externalRouteCallback: ExternalRouteResponseCallback?
    ): String {
        responses.values.find { it.conditions.url.pattern == url.pattern && it.conditions.requestMethod == method }?.let {
            throw IllegalStateException("Colliding mocks: $method $url")
        }

        val token = tokenNumber.incrementAndGet().toString()
        responses[token] = InterceptData(InterceptConditions(method, url), callback, externalRouteCallback)
        return token
    }

    private fun recordRequest(token: String, request: RecordedRequest) {
        val timedRecordedRequest = TimedRecordedRequest(System.currentTimeMillis(), request)

        recordedRequests[token] = recordedRequests.getOrDefault(token, listOf()) + timedRecordedRequest
        recordedRequestFlow.tryEmit(timedRecordedRequest)
    }

    fun unmockResponse(token: String) {
        responses -= token
        recordedRequests -= token
    }

}

val HttpUrl.tail: String
    get() {
        return StringBuilder().apply {
            encodedPath.also { path ->
                append(path.substring(1))
            }
            encodedQuery?.also {
                append('?')
                append(it)
            }
            encodedFragment?.also {
                append('#')
                append(it)
            }
        }.toString()
    }