package band.effective.headlines.compose.network.verifier


import band.effective.headlines.compose.network.Responder
import band.effective.headlines.compose.network.TimedRecordedRequest
import band.effective.headlines.compose.network.tail
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import okhttp3.mockwebserver.RecordedRequest
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

private val DEFAULT_AWAIT_TIMEOUT = 15.seconds

/**
 * Perform verification of mocked backend being called by test actions.
 *
 * There are optional filters that you can apply with "keyword" `with`.
 * See documentation for `with` for more info.
 *
 * Assertion can be made by `test` or `called` operation.
 *
 * `test` operation used to assert filtered calls. Almost all extensions from `with` are available there.
 * You should make **assertions** in this block.
 * Also this block verify that **at least one** request for verification made.
 *
 * `called` operation used to assert count of filtered requests.
 *
 * Don't forget about parameter `recheckRequests` in `endpoint`.
 *
 * Example:
 * ```
 * verifyBackend {
 *     endpoint(ENDPOINT_REGEX, RequestMethod.POST) with {
 *         url { queryParams["param1"] == "trap" }
 *     } test {
 *         url { assertTrue("Param2 should be 'hata'", queryParams["param2"] == "hata")
 *     }
 * }
 * ```
 */
fun verifyBackend(block: suspend MockBackendVerifyBlock.() -> Unit) =
    runBlocking { coVerifyBackend(block) }

suspend fun coVerifyBackend(block: suspend MockBackendVerifyBlock.() -> Unit) =
    block(MockBackendVerifyBlock())

@VerifyBackendMarker
class MockBackendVerifyBlock {

    /**
     * Endpoint to be checked. URL should match exactly "trail" of the request.
     *
     * For request `GET https://api.music.yandex.net/search?text=no%20daubt&type=all#lol`
     * trail is `search?text=no%20daubt&type=all#lol`
     *
     * @param url trail of the request URL
     * @param method filter for the request
     * @param timeout timeout for request checking
     * @param recheckRequests should requests be rechecked?
     */
    fun endpoint(
        url: Regex,
        method: Responder.RequestMethod = Responder.RequestMethod.ANY,
        timeout: Duration = DEFAULT_AWAIT_TIMEOUT,
        recheckRequests: Boolean = false
    ): SuspendVerifyEndpointBlock {
        return SuspendVerifyEndpointBlock(url, method, timeout, recheckRequests, emptyList())
    }
}

@VerifyEndpointMarker
class SuspendVerifyEndpointBlock(
    private val url: Regex,
    private val method: Responder.RequestMethod,
    private val timeout: Duration,
    private val recheckRequests: Boolean,
    private val initialFilters: List<Filter>
) {

    /**
     * Apply filters before assertions. All filters executed in given order. Filters under the hood using
     * short-circuit evaluation -- first filter with `false` as a result discontinue evaluation of other ones.
     *
     * There are some handy extensions for filters you can find in `filters.kt`
     */
    infix fun with(filters: FilterBlock.() -> Unit): FilteredEndpoint {
        val filterBlock = FilterBlock(initialFilters + listOf(
            MethodFilter(method),
            UrlFilter { this@SuspendVerifyEndpointBlock.url.matches(url.tail) }
        )).also(filters)

        filterBlock + AlreadyFilteredFilter(recheckRequests)

        return FilteredEndpoint(filterBlock, timeout)
    }
}

class FilterBlock(
    initialFilters: List<Filter> = emptyList()
) {

    private val _filters: MutableList<Filter> = initialFilters.toMutableList()

    internal val filters: List<Filter>
        get() = _filters

    operator fun plus(filter: Filter) {
        _filters += filter
    }
}

interface Filter {

    fun filter(request: RecordedRequest, time: Long): Boolean
}

class FilteredEndpoint(
    filters: FilterBlock,
    private val timeout: Duration
) {

    private val filters: List<Filter> = filters.filters
    private val checkedRequests = mutableListOf<RecordedRequest>()

    suspend infix fun called(verifier: FilteredEndpointVerifier) {
        Responder.getTimedRecordedRequests()
            .filter { it.filter(filters) }
            .forEach { (_, request) ->
                checkedRequests += request
                verifier.verifyRequestWithWrappedException(request)
            }
        if (verifier.isVerificationComplete())
            return

        if (!timeout.isPositive()) {
            verifier.throwIfNotComplete()
        }

        withTimeoutOrNull(timeout) {
            Responder.getTimedRecordedRequestFlow()
                .filter { it.filter(filters) }
                .takeWhile { (_, request) ->
                    checkedRequests += request
                    verifier.verifyRequestWithWrappedException(request)
                    !verifier.isVerificationComplete()
                }
                .collect()
        }
        verifier.throwIfNotComplete()
    }

    private suspend fun FilteredEndpointVerifier.throwIfNotComplete() {
        if (!isVerificationComplete()) {
            throw MockedBackendRequestsVerificationException(
                checkedRequests,
                getVerificationException()
            )
        }
    }

    private fun TimedRecordedRequest.filter(filters: List<Filter>): Boolean {
        return filters.find { filter -> !filter.filter(request, time) } == null
    }

    private suspend fun FilteredEndpointVerifier.verifyRequestWithWrappedException(request: RecordedRequest) {
        try {
            this.verifyRequest(request)
        } catch (t: Throwable) {
            throw MockedBackendRequestsVerificationException(checkedRequests, t)
        }
    }
}

interface FilteredEndpointVerifier {

    suspend fun verifyRequest(request: RecordedRequest)

    suspend fun isVerificationAvailable(): Boolean

    suspend fun isVerificationComplete(): Boolean

    suspend fun getVerificationException(): Exception
}

@DslMarker
annotation class VerifyBackendMarker

@DslMarker
annotation class VerifyEndpointMarker

class MockedBackendRequestsVerificationException(
    requests: Iterable<RecordedRequest>,
    cause: Throwable
) : RuntimeException(
    """
    |An error occurred during assertions: ${cause.message}
    |
    |Checked against requests:
    |
    |${requests.joinToErrorString()}
    """.trimMargin("|"), cause
)

private fun Iterable<RecordedRequest>.joinToErrorString() =
    joinToString("\n\n------\n\n") { request ->
        """
    |$request
    |${request.headers}
    |
    |${runOrEmpty { request.body.snapshot().utf8() }}
    """.trimMargin("|")
    }

private fun runOrEmpty(block: () -> String) = runCatching(block).getOrDefault("")
