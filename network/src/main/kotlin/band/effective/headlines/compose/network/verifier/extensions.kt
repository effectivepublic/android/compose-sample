package band.effective.headlines.compose.network.verifier

suspend infix fun FilteredEndpoint.test(assertion: suspend AssertionBlock.() -> Unit) =
    this called AssertVerifier(assertion)
