package band.effective.headlines.compose.network.verifier

import okhttp3.mockwebserver.RecordedRequest

fun FilterBlock.url(filter: UrlContext.() -> Boolean) {
    this + UrlFilter(filter)
}

class AssertionBlock(
    val request: RecordedRequest
) {

    suspend fun url(context: suspend UrlContext.() -> Unit) = context(UrlContext(request))
}

class AssertVerifier(
    private val assertion: suspend AssertionBlock.() -> Unit
) : FilteredEndpointVerifier {

    var isAsserted: Boolean = false

    override suspend fun verifyRequest(request: RecordedRequest) {
        isAsserted = true
        assertion(AssertionBlock(request))
    }

    override suspend fun isVerificationAvailable(): Boolean = !isAsserted

    override suspend fun isVerificationComplete(): Boolean = isAsserted

    override suspend fun getVerificationException(): Exception =
        IllegalStateException("No calls to assertion done")
}
