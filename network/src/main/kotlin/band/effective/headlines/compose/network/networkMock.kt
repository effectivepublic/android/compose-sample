package band.effective.headlines.compose.network

import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import okhttp3.Call
import okhttp3.HttpUrl
import okhttp3.Request
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicLong

class MockContext internal constructor(
    val originalRequest: Request,
    val recordedRequest: RecordedRequest,
    _originalCall: () -> Call
) {
    val originalCall: Call by lazy(_originalCall)
}

interface MockResponder {

    fun willRespond(request: Request): Boolean
    fun respond(mockContext: MockContext): MockResponse
}

interface ExternalRouteResponder {

    fun willRespond(request: RecordedRequest): Boolean
    fun respond(request: RecordedRequest): MockResponse
}

data class TimedRecordedRequest(
    val time: Long,
    val request: RecordedRequest
)

interface RequestRecorder {

    /**
     * Get the list of [RecordedRequest] and time (in milliseconds) [MockServer] received it,
     * made in the **current test** **BEFORE** the call of this method.
     */
    fun getTimedRecordedRequests(): List<TimedRecordedRequest>

    /**
     * Get real-time flow of the [RecordedRequest] and time (in milliseconds) [MockServer] received it,
     * made **AFTER** the call of this method **AND subscription** to this flow.
     * Given flow can buffer up to 5 [RecordedRequest] in case of the slow subscriber.
     */
    fun getTimedRecordedRequestFlow(): Flow<TimedRecordedRequest>
}

class MockServer(
    private val responder: MockResponder,
    private val externalRouteResponder: ExternalRouteResponder
) {

    companion object {
        private val context = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

        private const val BRIDGE_KEY_HEADER_NAME = "X-Mock-Bridge-Data-Key"
    }

    private val bridgeData = ConcurrentHashMap<String, BridgeData>()
    private val keyNumber = AtomicLong(1)

    private fun nextBridgeKey(): String = "bridgeKey${keyNumber.getAndIncrement()}"

    private val server = MockWebServer().also {
        it.dispatcher = MockDispatcher()
    }

    suspend fun createCallFactory(delegateFactory: Call.Factory): Call.Factory =
        withContext(context) {
            MockCallFactory(baseUrl(), delegateFactory)
        }

    suspend fun start() = withContext(context) {
        server.start()
    }

    suspend fun baseUrl(): HttpUrl = withContext(context) {
        server.url("/")
    }

    private class BridgeData(val request: Request, val originalCall: () -> Call)

    private inner class MockDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return request.headers[BRIDGE_KEY_HEADER_NAME]?.let { bridgeData.remove(it) }
                ?.let { bridgeData ->
                    responder.respond(
                        MockContext(
                            bridgeData.request,
                            request,
                            bridgeData.originalCall
                        )
                    )
                } ?: if (externalRouteResponder.willRespond(request)) {
                externalRouteResponder.respond(request)
            } else {
                MockResponse().setResponseCode(404)
            }
        }
    }

    private inner class MockCallFactory(
        private val baseUrl: HttpUrl,
        private val delegateFactory: Call.Factory
    ) : Call.Factory {

        private val apiHost: String = BuildConfig.NEWS_STAGE_API_KEY

        override fun newCall(request: Request): Call {
            try {
                if (!responder.willRespond(request)) {
                    if (request.url.host == apiHost) {
                        throw IllegalStateException("Got a request to the real API. Please, add new or fix existing mock backend for this request: $request")
                    }
                    return delegateFactory.newCall(request)
                }

                val originalCall = { delegateFactory.newCall(request) }

                val bridgeKey = nextBridgeKey()
                bridgeData[bridgeKey] = BridgeData(request, originalCall)

                val newUrl = request.url.newBuilder()
                    .host(baseUrl.host)
                    .port(baseUrl.port)
                    .scheme(baseUrl.scheme)
                    .build()

                val newRequest = request.newBuilder()
                    .header(BRIDGE_KEY_HEADER_NAME, bridgeKey)
                    .url(newUrl)
                    .build()

                return delegateFactory.newCall(newRequest)
            } catch (e: Throwable) {
                Timber.e(e, "Mock Response Fail %s", request.url)
                throw e
            }
        }
    }
}
