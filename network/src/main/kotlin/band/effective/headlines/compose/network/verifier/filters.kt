package band.effective.headlines.compose.network.verifier

import band.effective.headlines.compose.network.Responder
import okhttp3.mockwebserver.RecordedRequest
import java.util.concurrent.CopyOnWriteArraySet

class MethodFilter(
    private val method: Responder.RequestMethod
) : Filter {

    override fun filter(request: RecordedRequest, time: Long): Boolean {
        return request.method?.let { method.matches(it) } ?: false
    }
}

class UrlFilter(
    private val filter: UrlContext.() -> Boolean
) : Filter {

    override fun filter(request: RecordedRequest, time: Long): Boolean {
        return filter(UrlContext(request))
    }
}

internal class AlreadyFilteredFilter(
    private val shouldBypassAnyway: Boolean
) : Filter {

    override fun filter(request: RecordedRequest, time: Long): Boolean {
        return checkedRequests.add(request.identityHashCode) || shouldBypassAnyway
    }

    companion object {

        private val checkedRequests = CopyOnWriteArraySet<Int>()
    }
}

inline val Any.identityHashCode: Int get() = System.identityHashCode(this)
