package band.effective.headlines.compose.core.utils

import org.junit.rules.RuleChain
import org.junit.rules.TestRule

/**
 * ### Sample:
 * ```
 * ruleChain {
 *  +activityTestRule<MyActivity>()
 *  around(ActivityTestRule(MyActivity::class.java)
 * }
 * ```
 */
fun ruleChain(builder: RuleChainBuilder.() -> Unit): TestRule {
    return RuleChainBuilder().apply(builder).build()
}

class RuleChainBuilder internal constructor() {

    private var ruleChain = RuleChain.emptyRuleChain()

    operator fun TestRule.unaryPlus() = around(this)

    fun around(aroundRule: TestRule) {
        ruleChain = ruleChain.around(aroundRule)
    }

    internal fun build(): TestRule = ruleChain
}