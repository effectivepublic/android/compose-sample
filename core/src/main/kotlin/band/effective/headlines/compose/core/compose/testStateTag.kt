package band.effective.headlines.compose.core.compose

import androidx.compose.runtime.Stable
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.SemanticsPropertyKey
import androidx.compose.ui.semantics.SemanticsPropertyReceiver
import androidx.compose.ui.semantics.semantics
import band.effective.headlines.compose.core.compose.CustomSemanticsProperties.TestStateTag

/**
 * Applies a tag to allow modified element to be found in tests.
 *
 * This is a convenience method for a [semantics] that sets [SemanticsPropertyReceiver.testTag].
 */
@Stable
fun Modifier.testStateTag(tag: String = "default", value: Any) = semantics(
    properties = {
        testStateTag = mapOf(Pair(tag, value))
    }
)

@Stable
fun Modifier.testStateTag(dict: Map<String, Any>) = semantics(
    properties = {
        testStateTag = dict
    }
)

/*@VisibleForTesting*/
object CustomSemanticsProperties {

    val TestStateTag = SemanticsPropertyKey<Map<String, Any>>(
        name = "TestStateTag",
        mergePolicy = { parentValue, _ ->
            // Never merge TestTags, to avoid leaking internal test tags to parents.
            parentValue
        }
    )
}

/**
 * Test state tag attached to this semantics node.
 */
var SemanticsPropertyReceiver.testStateTag by TestStateTag

