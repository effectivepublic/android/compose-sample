package band.effective.headlines.compose.core.utils

import android.app.Instrumentation
import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry

val instrumentation: Instrumentation get() = InstrumentationRegistry.getInstrumentation()

val targetContext: Context get() = instrumentation.targetContext
val testContext: Context get() = instrumentation.context
