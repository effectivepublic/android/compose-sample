package band.effective.headlines.compose.core.compose

import androidx.compose.ui.test.SemanticsMatcher
import androidx.compose.ui.test.SemanticsNodeInteraction

/**
 * Проверяет есть ли в словаре с семантикой тестовых состояний запрашиваемое
 *
 * @param key key for the test state.
 * @param value state value.
 *
 * @see SemanticsProperties.TestTag
 */
fun hasTestStateTag(key: String, value: Any): SemanticsMatcher {
    return SemanticsMatcher("${CustomSemanticsProperties.TestStateTag.name} has '$key' -> '$value'") {
        val map = it.config.getOrElseNullable(CustomSemanticsProperties.TestStateTag) { null }
        map?.get(key) == value
    }
}

fun SemanticsNodeInteraction.assertStateTagIsEqualTo(
    key: String,
    value: Any
): SemanticsNodeInteraction {
    val map = fetchSemanticsNode().config
        .getOrElseNullable(CustomSemanticsProperties.TestStateTag) { null }
    if (map == null) {
        throw AssertionError("No TestStateTag defined in node")
    } else if (key !in map) {
        throw AssertionError("Key '$key' isn't defined in node")
    } else if (map[key] != value) {
        throw AssertionError("Expected $value, but got '${map[key]}'")
    }
    return this
}

/**
 * Проверяет есть ли в словаре с семантикой тестовых состояний запрашиваемое
 * В качестве ключа используется ключ по умолчанию
 *
 * @param value state value.
 *
 * @see SemanticsProperties.TestTag
 */
fun hasTestStateTag(value: Any): SemanticsMatcher {
    return hasTestStateTag("default", value)
}
