package band.effective.headlines.compose.core.utils

@Suppress("NOTHING_TO_INLINE")
inline fun <K, V : Any> Map<out K, V?>.filterNotNullValues(): Map<K, V> {
    val result = LinkedHashMap<K, V>()
    for (entry in this) {
        entry.value?.let {
            result.put(entry.key, it)
        }
    }
    return result
}
